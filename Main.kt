class Dog(age: Int, weight: Double) {
    var age: Int = age
    var weight: Double = weight

    fun showAgeOfDog() {
        println("Dog is $age years old.")
    }
}

fun main(args: Array<String>) {
    var firstDog = Dog(1, 2.0)
    var secondDog = Dog(2, 3.0)
    withDogIsOlder(firstDog, secondDog)
}

fun withDogIsOlder(left: Dog, right: Dog) {
    if (left.age > right.age) {
        println("First dog is older.")
        left.showAgeOfDog()
    } else {
        println("Second dog is older.")
        right.showAgeOfDog()
    }
}
